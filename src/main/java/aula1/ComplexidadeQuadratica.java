package aula1;

import java.util.ArrayList;
import java.util.Arrays;

public class ComplexidadeQuadratica {


    public static void main(String[] args) {
        ArrayList<String> items = new ArrayList<String>(Arrays.asList("a", "b",
                                                                       "c", "d",
                                                                       "e","f","g","h"));
        int contadorExecucões = 0;
        for (int a = 0; a < items.size(); a++) {
            for (int b = 0; b < items.size(); b++) {
                System.out.println(items.get(a) + " - " + items.get(b));
                contadorExecucões++;
            }
        }
        System.out.println("total de execuções: " + contadorExecucões);
    }
}