package aula1;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Arrays;

public class ComplexidadeLogaritmica {


    public static void main(String[] args) {
        int tamanhoMaximo = 1000;
        ArrayList<Integer> itens = preencherLista(tamanhoMaximo);
        int procurado =103;
        int first = 0;
        int last = itens.size() - 1;
        int midle;
        boolean localizado = false;
        int iteracoes = 0;
        while (first <= last && localizado != true) {
            iteracoes ++;
            midle = (first + last) / 2;
            if (itens.get(midle) == procurado) {
                localizado = true;
                System.out.println("localizado valor: "+ itens.get(midle) + "na posição: "+ midle);
            } else {
                if (itens.get(midle) < procurado) {
                    first = midle + 1;
                } else {
                    last = midle - 1;
                }
            }
        }
        System.out.println(iteracoes);
    }

    public static ArrayList<Integer> preencherLista(int tamanhoMaximo){
        ArrayList<Integer> itens = new ArrayList<Integer>();
        for (int a = 0; a < tamanhoMaximo-1; a++) {
            itens.add(a * 2);
        }
        itens.add((tamanhoMaximo * 2) + 5);
        return itens;
    }
}
