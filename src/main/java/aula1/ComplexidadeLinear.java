package aula1;

import java.util.ArrayList;
import java.util.Arrays;

public class ComplexidadeLinear {


    public static void main(String[] args) {
        ArrayList<Integer> items = new ArrayList<Integer>(
                                    Arrays.asList(2,4,6,8,10,11));
        int contadorExecucões = 0;
        int totalizador =0;
         for (int a = 0; a < items.size(); a++) {
                totalizador = totalizador + items.get(a);
                contadorExecucões++;
         }
        System.out.println("Valor total: " + totalizador);
        System.out.println("total de execuções: " + contadorExecucões);
    }

    public void meuMetodo(ArrayList<String> minhaLista, String valor){
        minhaLista.add(valor);
    }

}